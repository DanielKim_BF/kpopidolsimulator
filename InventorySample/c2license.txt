﻿License type: PERSONAL
Product: Construct 2
Licensed to: Hyung Chul Kim
Email: danielkim3d@gmail.com
Comment: 
Timestamp: 1462174179
Hash: 062E8933F4B0AB9A51354FADE6DAEA29E07606E0BBC3AA78

This is a standard license, for independent use by the above named individual up to an associated use revenue of 5000 USD, or educational, charity or otherwise non-commercial use.  Use outside of these bounds is prohibited.